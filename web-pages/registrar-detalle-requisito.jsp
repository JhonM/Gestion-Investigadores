<%-- 
    Document   : registro-usuario
    Created on : 24/06/2018, 06:01:08 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<h1 class="page-title">Registrar Detalle Requisito</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="form-basic.html">Detalle Requisito</a></li> 
    <li class="active"><strong>Registro</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Registro</h4>
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <form role="form" id="formRegistrar" action="return:false" autocomplete="off">
                    <div id="alert"></div>

                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="tRequisito">Tipo Requisito</label>
                            <select onchange="verDicClase(this.value)" class="form-control" id="tRequisito" name="tRequisito" required title="Seleccione una opcion"></select>
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="opcion">Opción</label>
                            <select class="form-control" id="opcion" name="opcion" required title="Seleccione una opcion"></select>
                        </div> 
                    </div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="sInvestigador">Subtipo Investigador</label>
                            <select onchange="" class="form-control" id="sInvestigador" name="sInvestigador" required title="Seleccione una opcion"></select>
                        </div>                       
                    </div> 
                    <div class="line-dashed"></div>

                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="tUsuario">Forma validar</label>
                            <select onchange="verTipoInvestigador(this.value)" class="form-control" id="tUsuario" name="tUsuario" required title="Seleccione una opcion"></select>
                        </div>
                        <div class="form-group col-lg-6" hidden="" id="divTinvestigador">                                                   
                            <label class="control-label" for="Nformacion">Nivel de formaciòn</label>
                            <select class="form-control" id="Nformacion" name="Nformacion" title="Seleccione una opcion"></select>
                        </div>
                        <div class="form-group col-lg-6" hidden id="divSinvestigador">                                                   
                            <label class="control-label" for="sProducto">Tipo Producto</label>
                            <select onchange="listarSubtipoProductoPorIdProducto('Producto', this.value); verDivStipoProducto(this.value);" class="form-control" id="sProducto" name="sProducto" title="Seleccione una opcion"></select>
                        </div>                         
                    </div> 

                    <div class="row" id="divClaseAnio" hidden>                                            
                        <div class="form-group col-lg-6" id="divClase" hidden>
                            <label class="control-label" for="clase">Clase</label>
                            <input type="text" class="form-control" id="clase" name="clase" placeholder="Clase" required maxlength="3" title="Ingrese cantidad">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="anio">Año</label>
                            <input type="tel" class="form-control" id="anio" name="anio" placeholder="Año" required maxlength="4" title="Ingrese año">
                        </div>                     
                    </div> 
                    <div class="row" id="divCantidad" hidden>                                           
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="cantidad">Cantidad</label>
                            <input type="tel" class="form-control" id="cantidad" name="cantidad" placeholder="Cantidad" required maxlength="2" title="Ingrese cantidad">
                        </div>
                        <div class="form-group col-lg-6" id="divStipoProducto">                                                   
                            <label class="control-label" for="Producto">Subtipo Producto</label>
                            <select class="form-control" id="Producto" name="Producto" title="Seleccione una opcion"><option>Seleccione una opción</option></select>
                        </div> 
                    </div>  

                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="reset" class="btn btn-white btn-rounded">Cancelar</button>
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formRegistrar', 1);">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="footer-main"> 
    &copy; 2016 <strong>Integral</strong> Admin Template by <a target="_blank" href="#/">G-axon</a>
</footer>	
<!-- /footer -->
<script>
    var operacion = null;
    $(document).ready(function () {
        listarTipoValida("tUsuario");
        listarNivelFormacion("Nformacion");
        listarTipoProducto("sProducto");
        listarTipoRequisito("tRequisito");
        listarOpcionRequisito("opcion");
        listarSubTipoInvestigador("sInvestigador");
    });

    $("input[type=tel]").on('input', function () {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    function registrarDetalleRequisito() {

        var dato1 = {
            idTipoRequisito: $("#tRequisito").val(),
            idOpcion: $("#opcion").val(),
            idsubTipoInvestigador: $("#sInvestigador").val(),
        };

        var cat = 1;
        if ($("#cantidad").val() != "") {
            cat = $("#cantidad").val();
        }

        var datos2 = {
            idTipoValida: $("#tUsuario").val(),
            idNivelFormacion: $("#Nformacion").val(),
            cantidad: cat,

            agrupadorClase: $("#clase").val(),
            tiempo: $("#anio").val(),
            idSubtipoProducto: $("#Producto").val(),

            idTipoProducto: $("#sProducto").val(),
        };
        ajaxGestion.registrarDetalleRequisito(dato1, datos2, {
            callback: function (data) {
                console.log(data);
                if (data) {
                    notificacion("alert-success", "Se <strong>registraron</strong> los datos on exíto.");
                    $("#formRegistrar")[0].reset();
                } else {
                    notificacion("alert-danger", "Error al <strong>registrar</strong> los datos.");
                }
            },
            timeout: 20000
        });
    }

    function notificacion(t, m) {
        if ($('#alert').text() == "") {
            setTimeout('$("#alert").text("")', 3000);
        }
        $("#alert").text("");
        $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                + '<span aria-hidden="true">×</span>'
                + '</button>'
                + m
                + '</div>');
        $("#alert").focus();

    }

    function listarSubtipoProductoPorIdProducto(idCombo, id) {
        ajaxGestion.listarSubtipoProductoPorIdProducto(id, {
            callback: function (data) {
                dwr.util.removeAllOptions(idCombo);
                dwr.util.addOptions(idCombo, [{
                        id: '',
                        descripcion: 'Seleccione una opción'
                    }], 'id', 'descripcion');
                if (data !== null) {
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function ejecutarPostValidate() {
        if (operacion == 1)
            registrarDetalleRequisito();
        // if (operacion == 2)
        operacion = null;
    }

    function listarSubTipoInvestigador(idCombo) {
        ajaxGestion.listarSubTipoInvestigador(1, {
            callback: function (data) {
                if (data !== null) {
                    var mostrar = [];
                    for (var i = 0; i < data.length; i++) {

                        if (data[i].id != "1") {
                            mostrar.push(data[i]);
                        }
                    }
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, mostrar, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarOpcionRequisito(idCombo) {
        ajaxGestion.listarOpcionRequisito({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarTipoRequisito(idCombo) {
        ajaxGestion.listarTipoRequisito({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarTipoValida(idCombo) {
        ajaxGestion.listarTipoValida({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarClaseProducto(idCombo) {
        ajaxGestion.listarClaseProducto({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    //jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarTipoProducto(idCombo, valorSeleccionado) {
        ajaxGestion.listarTipoProducto({
            callback: function (data) {
                dwr.util.removeAllOptions(idCombo);
                dwr.util.addOptions(idCombo, [{
                        id: '',
                        descripcion: 'Seleccione una opción'
                    }], 'id', 'descripcion');
                if (data !== null) {
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    jQuery("#" + idCombo).val(valorSeleccionado);
                }
            },
            timeout: 20000
        });
    }

    function listarNivelFormacion(idCombo, valorSeleccionado) {
        ajaxGestion.listarNivelFormacion({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione una opción'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    $("#" + idCombo).val(valorSeleccionado).trigger("change");
                }
            },
            timeout: 20000
        });
    }

    function verDicClase(id) {
        console.log($("#tUsuario").val(), id);
        if ($("#tUsuario").val() == "2" && id == "3") {
            $("#divClase").show();
            $('#clase').prop("required", true);
        } else {
            $("#divClase").hide();
            $('#clase').prop("required", true);
        }
    }

    function verDivStipoProducto(id) {
        console.log($("#tUsuario").val(), id);
        if ($("#tUsuario").val() == "2" && id == "4") {
            $("#divStipoProducto").show();
            $('#Producto').prop("required", true);
        } else {
            $("#divStipoProducto").hide();
            $('#Producto').prop("required", true);
        }
    }

    function verTipoInvestigador(id) {
        if (id == "2") {
            $("#divTinvestigador").hide();
            $("#divSinvestigador").show();
            $("#divClaseAnio").show();
            $("#divCantidad").show();
            $('#Nformacion').prop("required", false);
            $('#sProducto').prop("required", true);
            $('#clase').prop("required", true);
            $('#cantidad').prop("required", true);
            $('#anio').prop("required", true);

            if ($("#tRequisito").val() == "3") {
                $("#divClase").show();
                $('#clase').prop("required", true);
            } else {
                $("#divClase").hide();
                $('#clase').prop("required", true);
            }

            if ($("#sProducto").val() == "4") {
                $("#divStipoProducto").show();
                $('#Producto').prop("required", true);
            } else {
                $("#divStipoProducto").hide();
                $('#Producto').prop("required", true);
            }

        } else {
            $("#divSinvestigador").hide();
            $("#divTinvestigador").show();
            $("#divClaseAnio").hide();
            $("#divCantidad").hide();
            $('#clase').prop("required", false);
            $('#cantidad').prop("required", false);
            $('#anio').prop("required", false);
            $('#sProducto').prop("required", false);
            $('#Nformacion').prop("required", true);
        }

    }
</script>