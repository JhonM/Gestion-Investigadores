<%-- 
    Document   : index
    Created on : 22/06/2018, 10:27:14 AM
    Author     : Ing Hebert Medelo
--%>
<%@page import="co.usb.gestion.mvc.dto.PerfilDTO"%>
<%@page import="co.usb.gestion.mvc.dto.MenuDTO"%>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<MenuDTO> menu = null;
    PerfilDTO datosUsuario = (PerfilDTO) session.getAttribute("datosUsuario");
    if (datosUsuario.getMenu() != null) {
        menu = datosUsuario.getMenu();
    }
%>
<!DOCTYPE html>
<html lang="en">
    <!-- Mirrored from www.g-axon.com/integral-2.0/light/fixed-sidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 26 Oct 2016 16:27:40 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Integral - A fully responsive, HTML5 based admin template">
        <meta name="keywords" content="Responsive, Web Application, HTML5, Admin Template, business, professional, Integral, web design, CSS3">
        <title>Gestion CV | USB</title>
        <!-- Site favicon -->
        <link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico' />
        <!-- /site favicon -->
        <!-- Entypo font stylesheet -->
        <link href="css/entypo.css" rel="stylesheet">
        <!-- /entypo font stylesheet -->
        <!-- Font awesome stylesheet -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!-- /font awesome stylesheet -->
        <!-- Bootstrap stylesheet min version -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- /bootstrap stylesheet min version -->
        <!-- Integral core stylesheet -->
        <link href="css/integral-core.css" rel="stylesheet">
        <!-- /integral core stylesheet -->
        <link href="plugins/scrollbar/css/perfect-scrollbar.css" rel="stylesheet"> 

        <link href="css/integral-forms.css" rel="stylesheet"> 
    </head>
    <body>
        <!-- Loader Backdrop -->
        <div class="loader-backdrop">           
            <!-- Loader -->
            <div class="loader">
                <div class="bounce-1"></div>
                <div class="bounce-2"></div>
            </div>
            <!-- /loader -->
        </div>
        <!-- loader backgrop -->
        <!-- Page container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <div class="page-sidebar">
                <div class="sidebar-fixed">
                    <!-- Site header  -->
                    <header class="site-header">
                        <div class="site-logo"><a href="index.jsp"><img src="images/logo2.png" alt="Integral" title="Integral"  width="200" height="40"></a></div>
                        <div class="sidebar-collapse hidden-xs"><a class="sidebar-collapse-icon" href="#"><i class="icon-menu"></i></a></div>
                        <div class="sidebar-mobile-menu visible-xs"><a data-target="#side-nav" data-toggle="collapse" class="mobile-menu-icon" href="#"><i class="icon-menu"></i></a></div>
                    </header>
                    <!-- /site header -->
                    <!-- Main navigation -->
                    <ul id="side-nav" class="main-menu navbar-collapse collapse">
                        <li class="navigation-header">MEN�</li> 
                            <%for (int i = 0; i < menu.size(); i++) {%>
                        <li class="has-sub"><a href="collapsed-sidebar.html"><i class="<%=menu.get(i).getIconoMenu()%>"></i><span class="title"><%=menu.get(i).getTituloMenu()%></span></a>
                            <ul class="nav collapse">
                                <%for (int r = 0; r < menu.get(i).getFuncionalidad().size(); r++) {%>
                                <li class="" id="sudMenu<%=menu.get(i).getFuncionalidad().get(r).getId()%>" onclick="javascript:addActive(this.id);"><a href="<%=menu.get(i).getFuncionalidad().get(r).getPagina()%>"><span class="title"><%=menu.get(i).getFuncionalidad().get(r).getTitulo()%></span></a></li>
                                        <%}%>
                            </ul>
                        </li> 
                        <%}%>
                    </ul>
                </div>
                <!-- /main navigation -->		
            </div>
            <!-- /page sidebar -->
            <!-- Main container -->
            <div class="main-container">
                <!-- Main header -->
                <div class="main-header row">
                    <div class="col-sm-6 col-xs-7">
                        <!-- User info -->
                        <ul class="user-info pull-left">          
                            <li class="profile-info dropdown"><a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"> <img width="44" class="img-circle avatar" alt="" src="images/man-3.jpg"><%=datosUsuario.getNombre() + " " + datosUsuario.getApellido()%><span class="caret"></span></a>
                                <!-- User action menu -->
                                <ul class="dropdown-menu">                                   
                                    <li onclick="limpiarsimulador();"><a href="login.jsp"><i class="icon-logout"></i>Salir</a></li>
                                </ul>
                                <!-- /user action menu -->
                            </li>
                        </ul>
                        <!-- /user info -->
                    </div>

                </div>               <!-- /main header -->
                <!-- Main content -->
                <div class="main-content" id="container"></div>
                <!-- /main content -->
            </div>
            <!-- /main container -->
        </div>
        <!-- /page container -->
        <!--Load JQuery-->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="plugins/metismenu/js/jquery.metisMenu.js"></script>
        <script src="plugins/scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="plugins/scrollbar/js/scrollbar-script.js"></script>
        <script src="js/functions.js"></script>
        <script src="js/loader.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/interface/ajaxGestion.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/interface/ajaxSeguridad.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/engine.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/util.js"></script>
        <script>

                                    //cargarPagina("ver-nivel.jsp");
                                    function addActive(id) {
                                        $("#side-nav li ul li").each(function () {
                                            $(this).removeClass("active");
                                        });
                                        $("#" + id).addClass("active");
                                    }

                                    function cargarPagina(pagina) {
                                        $('#container').load(pagina);
                                    }
                                    
                                    function limpiarsimulador() {
                                        ajaxGestion.limpiarsimulador();
                                    }

                                    function validar(r, i) {
                                        operacion = i;
                                        jQuery('#' + r).validate({
                                            highlight: function (label) {
                                                jQuery(label).closest('.form-group').removeClass('has-success').addClass('has-error');
                                            },
                                            success: function (label) {
                                                jQuery(label).closest('.form-group').removeClass('has-error');
                                                label.remove();
                                            },
                                            errorPlacement: function (error, element) {
                                                var placement = element.closest('.input-group');
                                                if (!placement.get(0)) {
                                                    placement = element;
                                                }
                                                if (error.text() !== '') {
                                                    placement.after(error);
                                                }
                                            },
                                            submitHandler: function () {
                                                ejecutarPostValidate();
                                            }
                                        });
                                    }
        </script>   
    </body>
    <!-- Mirrored from www.g-axon.com/integral-2.0/light/fixed-sidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 26 Oct 2016 16:27:47 GMT -->
</html>