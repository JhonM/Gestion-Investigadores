<%@page import="co.usb.gestion.mvc.dto.PerfilDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    PerfilDTO datosUsuario = (PerfilDTO) session.getAttribute("datosUsuario");

%>

<!DOCTYPE html>
<h1 class="page-title">Simulacion Consulta Nivel Investigador</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
<!--    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="form-basic.html">Revisar</a></li> 
    <li class="active"><strong>Ver Nivel</strong></li> -->
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Simulacion del Nivel del Investigador:&nbsp;&nbsp;&nbsp;</h4>
                <h4 id="nivel" class="panel-title" style="color:#148e40"></h4>
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <form role="form" id="formRegistrar" action="return:false" autocomplete="off">

                    <table class="table">
                        <thead>
                            <tr>
                                <th># Requisitos</th>
                                <th>Productos Faltantes</th>                               
                            </tr>
                        </thead>
                        <tbody id="rd"></tbody>

                    </table>



                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="verNivel('<%= datosUsuario.getIdPerfil()%>');">Ver Nivel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="footer-main"> 
    &copy; 2016 <strong>Integral</strong> Admin Template by <a target="_blank" href="#/">G-axon</a>
</footer>	
<!-- /footer -->
<script>
    $(document).ready(function () {

    <% System.out.println("datos " + datosUsuario.toStringJson());%>

    });

    function verNivel(idPerfil) {

        ajaxGestion.verNivelInvestigadorSimulador(idPerfil, {
            callback: function (data) {
                if (data.observacion != null) {
                    
                    $("#rd").html("");
                    $("#nivel").html(data.descripcionNivel);
                    console.log("lo que llega ", data);


                    console.log("XXX", data.observacion);

                    var separador = "|";
                    var arregloDeSubCadenas = [];
                    var mostrar = data.observacion;
                    arregloDeSubCadenas = mostrar.split(separador);

                    console.log("XXX", arregloDeSubCadenas);





                    for (var i = 0; i < arregloDeSubCadenas.length; i++) {



                        console.log("R", arregloDeSubCadenas[i]);

                        console.log("D", arregloDeSubCadenas[i + 1]);

                        $("#rd").append("<tr><td>" + arregloDeSubCadenas[i] + "</td><td>" + arregloDeSubCadenas[i + 1] + "</td></tr>");

                        i++;

                    }



                    console.log(data);


                }
            }
        });
    }
</script>
