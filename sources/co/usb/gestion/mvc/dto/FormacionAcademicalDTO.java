/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dto;

import co.usb.gestion.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class FormacionAcademicalDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String fecha = Generales.EMPTYSTRING;
    String graduacion = Generales.EMPTYSTRING; 
    String nivelid = Generales.EMPTYSTRING;
    String nivel = Generales.EMPTYSTRING;
    String institucion = Generales.EMPTYSTRING;
    String programa = Generales.EMPTYSTRING;
    String perfilid = Generales.EMPTYSTRING;
    String perfil = Generales.EMPTYSTRING;
    String usuario = Generales.EMPTYSTRING;
    ArrayList<MenuDTO> menu = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getGraduacion() {
        return graduacion;
    }

    public void setGraduacion(String graduacion) {
        this.graduacion = graduacion;
    }
    
    public String getPerfilID() {
        return perfilid;
    }

    public void setPerfilID(String perfilid) {
        this.perfilid = perfilid;
    }
    
    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getNivelID() {
        return nivelid;
    }

    public void setNivelID(String nivelid) {
        this.nivelid = nivelid;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public ArrayList<MenuDTO> getMenu() {
        return menu;
    }

    public void setMenu(ArrayList<MenuDTO> menu) {
        this.menu = menu;
    }

    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
