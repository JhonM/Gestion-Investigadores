/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dto;

import co.usb.gestion.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class MenuDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String tituloMenu = Generales.EMPTYSTRING;
    String iconoMenu = Generales.EMPTYSTRING;
    ArrayList<FuncionalidadDTO> funcionalidad = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTituloMenu() {
        return tituloMenu;
    }

    public void setTituloMenu(String tituloMenu) {
        this.tituloMenu = tituloMenu;
    }

    public String getIconoMenu() {
        return iconoMenu;
    }

    public void setIconoMenu(String iconoMenu) {
        this.iconoMenu = iconoMenu;
    }

    public ArrayList<FuncionalidadDTO> getFuncionalidad() {
        return funcionalidad;
    }

    public void setFuncionalidad(ArrayList<FuncionalidadDTO> funcionalidad) {
        this.funcionalidad = funcionalidad;
    }

    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
