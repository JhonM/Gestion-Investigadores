/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.PerfilDTO;
import co.usb.gestion.mvc.dto.UsuarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class PerfilDAO {

    /**
     *
     * @param conexion
     * @param usuario
     * @return
     */
    public PerfilDTO consultarDatosUsuarioLogueado(Connection conexion, String usuario) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        PerfilDTO datosUsuario = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT p.perf_id, p.perf_nombre, p.perf_apellido, p.perf_cedula, p.perf_direccion, p.perf_correo, p.perf_telefono,  p.perf_sexo, ");
            cadSQL.append(" u.usua_id, u.usua_estado, u.usua_usuario, ");
            cadSQL.append(" t.tius_id, t.tius_descripcion, p.perf_orcid  ");
            cadSQL.append(" FROM perfil AS p ");
            cadSQL.append(" INNER JOIN usuario AS u ON u.usua_id = p.usua_id AND u.usua_usuario = ? ");
            cadSQL.append(" INNER JOIN tipo_usuario AS t ON u.tius_id = t.tius_id ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, usuario, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                datosUsuario = new PerfilDTO();
                datosUsuario.setNombre(rs.getString("p.perf_nombre"));
                datosUsuario.setApellido(rs.getString("p.perf_apellido"));
                datosUsuario.setDocumento(rs.getString("p.perf_cedula"));
                datosUsuario.setDireccion(rs.getString("p.perf_direccion"));
                datosUsuario.setCorreo(rs.getString("p.perf_correo"));
                datosUsuario.setSexo(rs.getString("p.perf_sexo"));
                datosUsuario.setId(rs.getString("u.usua_id"));
                datosUsuario.setIdTipoUsuario(rs.getString("t.tius_id"));
                datosUsuario.setTipoUsuario(rs.getString("t.tius_descripcion"));
                datosUsuario.setTelefono(rs.getString("p.perf_telefono"));
                datosUsuario.setEstado(rs.getString("u.usua_estado"));
                datosUsuario.setUsuario(rs.getString("u.usua_usuario"));
                datosUsuario.setIdPerfil(rs.getString("p.perf_id"));
                datosUsuario.setOrcid(rs.getString("p.perf_orcid"));
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return datosUsuario;
    }

    /**
     *
     * @param conexion
     * @param documento
     * @return
     */
    public boolean validarDocumento(Connection conexion, String documento) {

        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<PerfilDTO> listarUsuarios = null;
        boolean usuarioValido = false;

        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT perf_id, perf_cedula");
            cadSQL.append(" FROM perfil ");
            cadSQL.append(" WHERE  perf_cedula = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, documento, ps);

            rs = ps.executeQuery();

            listarUsuarios = new ArrayList();

            while (rs.next()) {
                usuarioValido = true;
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return usuarioValido;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listarUsuarios != null && listarUsuarios.isEmpty()) {
                    listarUsuarios = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return usuarioValido;
            }
        }

        return usuarioValido;
    }

    /**
     *
     * @param conexion
     * @param perfil
     * @return
     */
    public boolean registrarPerfil(Connection conexion, PerfilDTO perfil) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO perfil (perf_nombre, perf_apellido, perf_cedula, perf_direccion, perf_correo, perf_telefono, perf_sexo, perf_orcid, "
                    + " usua_id, suin_id, prac_id, tipr_id ) ");
            cadSQL.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, perfil.getNombre(), ps);
            AsignaAtributoStatement.setString(2, perfil.getApellido(), ps);
            AsignaAtributoStatement.setString(3, perfil.getDocumento(), ps);
            AsignaAtributoStatement.setString(4, perfil.getDireccion(), ps);
            AsignaAtributoStatement.setString(5, perfil.getCorreo(), ps);
            AsignaAtributoStatement.setString(6, perfil.getTelefono(), ps);
            AsignaAtributoStatement.setString(7, perfil.getSexo(), ps);
            AsignaAtributoStatement.setString(8, perfil.getOrcid(), ps);
            AsignaAtributoStatement.setString(9, perfil.getIdUsuario(), ps);
            AsignaAtributoStatement.setString(10, perfil.getIdSubtipoInvesstigador(), ps);
            AsignaAtributoStatement.setString(11, perfil.getIdProgramaAcademico(), ps);
            AsignaAtributoStatement.setString(12, perfil.getIdTipoProfesor(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    perfil.setIdUsuario(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param condicion
     * @return
     */
    public ArrayList<PerfilDTO> consultarPerfilPorNombreDocumento(Connection conexion, String condicion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilDTO> listado = null;
        PerfilDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.perf_id, c.perf_nombre, c.perf_apellido, c.perf_cedula, c.perf_direccion, c.perf_correo, c.perf_telefono, ");
            cadSQL.append(" c.perf_sexo, c.usua_id, c.suin_id, st.tiin_id, ");
            cadSQL.append(" s.usua_usuario, s.tius_id, s.usua_estado, t.tius_descripcion, c.perf_orcid ");
            cadSQL.append(" FROM perfil as c ");
            cadSQL.append(" INNER JOIN usuario AS s ON s.usua_id = c.usua_id ");
            cadSQL.append(" INNER JOIN tipo_usuario AS t ON t.tius_id = s.tius_id ");
            cadSQL.append(" LEFT JOIN subtipo_investigador AS st ON st.suin_id = c.suin_id ");
            cadSQL.append(" WHERE concat_ws(' ', c.perf_nombre, c.perf_apellido, c.perf_cedula ) like '%" + condicion + "%' ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilDTO();
                datos.setId(rs.getString("c.perf_id"));
                datos.setNombre(rs.getString("c.perf_nombre"));
                datos.setApellido(rs.getString("c.perf_apellido"));
                datos.setDocumento(rs.getString("c.perf_cedula"));
                datos.setDireccion(rs.getString("c.perf_direccion"));
                datos.setCorreo(rs.getString("c.perf_correo"));
                datos.setTelefono(rs.getString("c.perf_telefono"));
                datos.setSexo(rs.getString("c.perf_sexo"));
                datos.setIdUsuario(rs.getString("c.usua_id"));
                datos.setIdSubtipoInvesstigador(rs.getString("c.suin_id"));
                datos.setUsuario(rs.getString("s.usua_usuario"));
                datos.setIdTipoUsuario(rs.getString("s.tius_id"));
                datos.setEstado(rs.getString("s.usua_estado"));
                datos.setTipoUsuario(rs.getString("t.tius_descripcion"));
                datos.setIdTipoInvestigador(rs.getString("st.tiin_id"));
                datos.setOrcid(rs.getString("c.perf_orcid"));
                listado.add(datos);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param documento
     * @return
     */
    public UsuarioDTO recuperarContrasenia(Connection conexion, String documento) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        UsuarioDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT usua_id, perf_nombre, perf_apellido, perf_correo FROM perfil ");
            cadSQL.append(" WHERE  perf_cedula = ?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, documento, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                datos = new UsuarioDTO();
                datos.setId(rs.getString("usua_id"));
                datos.setNombre(rs.getString("perf_nombre") + " " + rs.getString("perf_apellido"));
                datos.setCorreo(rs.getString("perf_correo"));
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return datos;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarPerfil(Connection conexion, PerfilDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE perfil SET perf_nombre = ?, perf_apellido = ?, perf_direccion = ?, perf_telefono = ?, perf_sexo = ?, suin_id = ?, perf_orcid = ? ");
            cadSQL.append(" WHERE perf_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getNombre(), ps);
            AsignaAtributoStatement.setString(2, datos.getApellido(), ps);
            AsignaAtributoStatement.setString(3, datos.getDireccion(), ps);
            AsignaAtributoStatement.setString(4, datos.getTelefono(), ps);
            AsignaAtributoStatement.setString(5, datos.getSexo(), ps);
            AsignaAtributoStatement.setString(6, datos.getIdSubtipoInvesstigador(), ps);
            AsignaAtributoStatement.setString(7, datos.getOrcid(), ps);
            AsignaAtributoStatement.setString(8, datos.getId(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param condicion
     * @return
     */
    public ArrayList<PerfilDTO> consultarGestionPerfilProducto(Connection conexion, String condicion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilDTO> listado = null;
        PerfilDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.perf_id, c.perf_nombre, c.perf_apellido, c.perf_cedula, c.perf_direccion, c.perf_correo, c.perf_telefono, ");
            cadSQL.append(" c.perf_sexo, c.usua_id, c.suin_id,c.perf_orcid, ");
            cadSQL.append(" s.usua_usuario, s.tius_id, s.usua_estado, t.tius_descripcion, ");
            //informacion  formacion academica
            cadSQL.append(" foac.foac_id, foac.foac_anioinicio, foac.foac_aniograduacion, foac.foac_institucion, foac.foac_programaacademico, ");
            //informacion nivel de formacion
            cadSQL.append(" nifo.nifo_id, nifo.nifo_descripcion, nifo.nifo_estado, ");
            //subtipo investigador
            cadSQL.append(" suti.suin_codigo, suti.suin_descripcion ");
            cadSQL.append(" FROM perfil as c ");
            cadSQL.append(" INNER JOIN usuario AS s ON s.usua_id = c.usua_id ");
            cadSQL.append(" INNER JOIN tipo_usuario AS t ON t.tius_id = s.tius_id ");
            cadSQL.append(" LEFT JOIN formacion_academica AS foac ON foac.perf_id = c.perf_id  ");
            cadSQL.append(" LEFT JOIN nivel_formacion AS nifo ON nifo.nifo_id = foac.nifo_id ");
            cadSQL.append(" LEFT JOIN subtipo_investigador AS suti ON c.suin_id = suti.suin_id ");
            cadSQL.append(" WHERE concat_ws(' ', c.perf_nombre, c.perf_apellido, c.perf_cedula ) like '%" + condicion + "%' ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilDTO();
                datos.setId(rs.getString("c.perf_id"));
                datos.setNombre(rs.getString("c.perf_nombre"));
                datos.setApellido(rs.getString("c.perf_apellido"));
                datos.setDocumento(rs.getString("c.perf_cedula"));
                datos.setDireccion(rs.getString("c.perf_direccion"));
                datos.setCorreo(rs.getString("c.perf_correo"));
                datos.setTelefono(rs.getString("c.perf_telefono"));
                datos.setSexo(rs.getString("c.perf_sexo"));
                datos.setIdUsuario(rs.getString("c.usua_id"));
                datos.setIdSubtipoInvesstigador(rs.getString("c.suin_id"));
                datos.setObservacion(rs.getString("c.perf_observacion"));
                datos.setUsuario(rs.getString("s.usua_usuario"));
                datos.setIdTipoUsuario(rs.getString("s.tius_id"));
                datos.setEstado(rs.getString("s.usua_estado"));
                datos.setTipoUsuario(rs.getString("t.tius_descripcion"));
                datos.setIdFormacionAcademica(rs.getString("foac.foac_id"));
                datos.setAnioInicioFA(rs.getString("foac.foac_anioinicio"));
                datos.setAnioGraduacionFA(rs.getString("foac.foac_aniograduacion"));
                datos.setInstitucionFA(rs.getString("foac.foac_institucion"));
                datos.setProgramaAcademicoFA(rs.getString("foac.foac_programaacademico"));
                datos.setIdNivelFormacion(rs.getString("nifo.nifo_id"));
                datos.setDescripcionNivelFo(rs.getString("nifo.nifo_descripcion"));
                datos.setEstadoNivelFo(rs.getString("nifo.nifo_estado"));
                datos.setCodigoSutiInvestigador(rs.getString("suti.suin_codigo"));
                datos.setDescripcionSutiInvestigador(rs.getString("suti.suin_descripcion"));
                datos.setOrcid(rs.getString("c.perf_orcid"));

                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<PerfilDTO> listarUsuarios(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilDTO> listado = null;
        PerfilDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.perf_id, c.perf_nombre, c.perf_apellido, c.perf_cedula, c.perf_direccion, c.perf_correo, c.perf_telefono, ");
            cadSQL.append(" c.perf_sexo, c.usua_id, c.suin_id, st.tiin_id, st.suin_descripcion, ");
            cadSQL.append(" s.usua_usuario, s.tius_id, s.usua_estado, t.tius_descripcion, c.perf_orcid ");
            cadSQL.append(" FROM perfil as c ");
            cadSQL.append(" INNER JOIN usuario AS s ON s.usua_id = c.usua_id ");
            cadSQL.append(" INNER JOIN tipo_usuario AS t ON t.tius_id = s.tius_id ");
            cadSQL.append(" LEFT JOIN subtipo_investigador AS st ON st.suin_id = c.suin_id ");
            //cadSQL.append(" WHERE concat_ws(' ', c.perf_nombre, c.perf_apellido, c.perf_cedula ) like '%" + condicion + "%' ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilDTO();
                datos.setId(rs.getString("c.perf_id"));
                datos.setNombre(rs.getString("c.perf_nombre"));
                datos.setApellido(rs.getString("c.perf_apellido"));
                datos.setDocumento(rs.getString("c.perf_cedula"));
                datos.setDireccion(rs.getString("c.perf_direccion"));
                datos.setCorreo(rs.getString("c.perf_correo"));
                datos.setTelefono(rs.getString("c.perf_telefono"));
                datos.setSexo(rs.getString("c.perf_sexo"));
                datos.setIdUsuario(rs.getString("c.usua_id"));
                datos.setIdSubtipoInvesstigador(rs.getString("c.suin_id"));
                datos.setIdSubtipoInvesstigador(rs.getString("st.suin_descripcion"));
                datos.setUsuario(rs.getString("s.usua_usuario"));
                datos.setIdTipoUsuario(rs.getString("s.tius_id"));
                datos.setEstado(rs.getString("s.usua_estado"));
                datos.setTipoUsuario(rs.getString("t.tius_descripcion"));
                datos.setIdTipoInvestigador(rs.getString("st.tiin_id"));
                datos.setOrcid(rs.getString("c.perf_orcid"));

                listado.add(datos);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param perfil
     * @param nivel
     * @param observacion
     * @return
     */
    public boolean actualizarPerfilDesdeConsultarNivel(Connection conexion, String perfil, String nivel, String observacion) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE perfil SET suin_id = ?, perf_observacion = ?");
            cadSQL.append(" WHERE perf_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, nivel, ps);
            AsignaAtributoStatement.setString(2, observacion, ps);
            AsignaAtributoStatement.setString(3, perfil, ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param idPerfil
     * @return
     */
    public PerfilDTO consultarObservacion(Connection conexion, String idPerfil) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        PerfilDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.perf_id, c.perf_observacion ");
            cadSQL.append(" FROM perfil as c ");
            cadSQL.append(" WHERE c.perf_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idPerfil, ps);
            rs = ps.executeQuery();
            //listado = new ArrayList();

            if (rs.next()) {
                datos = new PerfilDTO();
                datos.setId(rs.getString("c.perf_id"));
                datos.setObservacion(rs.getString("c.perf_observacion"));
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return datos;
    }
    
    
     /**
     *
     * @param conexion
     * @return
     */
    
    public ArrayList<PerfilDTO> listarPerfilInvestigador(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilDTO> listado = null;
        PerfilDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT p.perf_id,CONCAT(p.perf_nombre,' ',p.perf_apellido) nombre " +
                            "  FROM perfil p INNER JOIN usuario u ON p.usua_id = u.usua_id " +
                            "  INNER JOIN tipo_usuario tu ON u.tius_id = tu.tius_id " +
                            "  WHERE tu.tius_descripcion = 'Investigador'");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilDTO();
                datos.setId(rs.getString("p.perf_id"));
                datos.setNombre(rs.getString("nombre"));
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
    
     /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<PerfilDTO> listarUsuariosInvestigadores(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilDTO> listado = null;
        PerfilDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT p.perf_id,CONCAT(p.perf_nombre,' ',p.perf_apellido) nombre " +
                            "  FROM perfil p INNER JOIN usuario u ON p.usua_id = u.usua_id " +
                            "  INNER JOIN tipo_usuario tu ON u.tius_id = tu.tius_id " +
                            "  WHERE tu.tius_descripcion = 'Investigador'");
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.perf_id, c.perf_nombre, c.perf_apellido, c.perf_cedula, c.perf_direccion, c.perf_correo, c.perf_telefono, ");
            cadSQL.append(" c.perf_sexo, c.usua_id, c.suin_id, ");
            cadSQL.append(" s.usua_usuario, s.tius_id, s.usua_estado, t.tius_descripcion, c.perf_orcid ");
            cadSQL.append(" FROM perfil as c ");
            cadSQL.append(" INNER JOIN usuario AS s ON s.usua_id = c.usua_id ");
            cadSQL.append(" INNER JOIN tipo_usuario AS t ON t.tius_id = s.tius_id ");
            cadSQL.append(" WHERE s.tius_id = 2 ");
            //cadSQL.append(" WHERE concat_ws(' ', c.perf_nombre, c.perf_apellido, c.perf_cedula ) like '%" + condicion + "%' ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilDTO();
                datos.setId(rs.getString("p.perf_id"));
                datos.setNombre(rs.getString("nombre"));
                listado.add(datos);
            }

            ps.close();
            ps = null;

                datos.setId(rs.getString("c.perf_id"));
                datos.setNombre(rs.getString("c.perf_nombre"));
                datos.setApellido(rs.getString("c.perf_apellido"));
                datos.setDocumento(rs.getString("c.perf_cedula"));
                datos.setDireccion(rs.getString("c.perf_direccion"));
                datos.setCorreo(rs.getString("c.perf_correo"));
                datos.setTelefono(rs.getString("c.perf_telefono"));
                datos.setSexo(rs.getString("c.perf_sexo"));
                datos.setIdUsuario(rs.getString("c.usua_id"));
                datos.setIdSubtipoInvesstigador(rs.getString("c.suin_id"));
                datos.setUsuario(rs.getString("s.usua_usuario"));
                datos.setIdTipoUsuario(rs.getString("s.tius_id"));
                datos.setEstado(rs.getString("s.usua_estado"));
                datos.setTipoUsuario(rs.getString("t.tius_descripcion"));
                datos.setOrcid(rs.getString("c.perf_orcid"));
                listado.add(datos);
            
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
    
    /**
     *
     * @param conexion
     * @return
     */
    public boolean limpiarsimulador(Connection conexion, String idPerfil) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" DELETE FROM temporal WHERE perf_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, idPerfil, ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

}
