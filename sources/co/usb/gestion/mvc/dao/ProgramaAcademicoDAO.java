/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.ProgramaAcademicoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class ProgramaAcademicoDAO {

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<ProgramaAcademicoDTO> listarProgramaAcademico(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ProgramaAcademicoDTO> listado = null;
        ProgramaAcademicoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT prac_id, prac_descripcion, prac_estado FROM programa_academico ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new ProgramaAcademicoDTO();
                datos.setId(rs.getString("prac_id"));
                datos.setDescripcion(rs.getString("prac_descripcion"));
                datos.setEstado(rs.getString("prac_estado"));

                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
}
