/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.SubTipoInvestigadorDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class SubTipoInvestigadorDAO {

    /**
     * @param idTipoInv
     * @return
     */
    public ArrayList<SubTipoInvestigadorDTO> listarSubTipoInvestigador(Connection conexion, String idTipoInv) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList listado = null;
        SubTipoInvestigadorDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT suin_id, tiin_id, suin_codigo, suin_descripcion, suin_estado FROM subtipo_investigador WHERE  tiin_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idTipoInv, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new SubTipoInvestigadorDTO();
                datos.setId(rs.getString("suin_id"));
                datos.setIdTipoInvestigador(rs.getString("tiin_id"));
                datos.setIdTipoInvestigador(rs.getString("suin_codigo"));
                datos.setDescripcion(rs.getString("suin_descripcion"));
                datos.setEstado(rs.getString("suin_estado"));
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return listado;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarSubtipoInvestigador(Connection conexion, SubTipoInvestigadorDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO subtipo_investigador (tiin_id, suin_codigo, suin_descripcion, suin_estado) ");
            cadSQL.append(" VALUES (?, ?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getIdTipoInvestigador(), ps);
            AsignaAtributoStatement.setString(2, datos.getCodigo(), ps);
            AsignaAtributoStatement.setString(3, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(4, datos.getEstado(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    datos.setId(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     * @param conexion
     * @return
     */
    public ArrayList<SubTipoInvestigadorDTO> listarTodosSubTipoInvestigador(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList listado = null;
        SubTipoInvestigadorDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT s.suin_id, s.tiin_id, s.suin_codigo, s.suin_descripcion, s.suin_estado, t.tiin_descripcion FROM subtipo_investigador as s  ");
            cadSQL.append(" INNER JOIN tipo_investigador t ON t.tiin_id = s.tiin_id  ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new SubTipoInvestigadorDTO();
                datos.setId(rs.getString("s.suin_id"));
                datos.setIdTipoInvestigador(rs.getString("s.tiin_id"));
                datos.setCodigo(rs.getString("s.suin_codigo"));
                datos.setDescripcion(rs.getString("s.suin_descripcion"));
                datos.setEstado(rs.getString("s.suin_estado"));
                datos.setTipoInvestigador(rs.getString("t.tiin_descripcion"));
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarSubipoInvestigador(Connection conexion, SubTipoInvestigadorDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE subtipo_investigador SET tiin_id = ?, suin_codigo = ?, suin_descripcion = ?, suin_estado = ? WHERE suin_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getIdTipoInvestigador(), ps);
            AsignaAtributoStatement.setString(2, datos.getCodigo(), ps);
            AsignaAtributoStatement.setString(3, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(4, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(5, datos.getId(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }
}
