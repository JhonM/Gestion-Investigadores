/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.UsuarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class UsuarioDAO {

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarUsuario(Connection conexion, UsuarioDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;

        boolean registroExitoso = false;
        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO usuario(usua_usuario, usua_contrasenia, tius_id, usua_estado )");
            cadSQL.append(" VALUES (?, SHA2(?,256), ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getUsuario(), ps);
            AsignaAtributoStatement.setString(2, datos.getClave(), ps);
            AsignaAtributoStatement.setString(3, datos.getIdTipoUsuario(), ps);
            AsignaAtributoStatement.setString(4, datos.getEstado(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    datos.setId(rs.getString(1));
                }
                rs.close();
                rs = null;
            }

        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;

    }

    /**
     *
     * @param conexion
     * @param usuario
     * @return
     */
    public boolean validarUsuario(Connection conexion, String usuario) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<UsuarioDTO> listarUsuarios = null;
        boolean usuarioValido = false;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT usua_id, usua_usuario");
            cadSQL.append(" FROM usuario ");
            cadSQL.append(" WHERE  usua_usuario = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, usuario, ps);
            rs = ps.executeQuery();
            listarUsuarios = new ArrayList();
            while (rs.next()) {
                usuarioValido = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return usuarioValido;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listarUsuarios != null && listarUsuarios.isEmpty()) {
                    listarUsuarios = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return usuarioValido;
            }
        }
        return usuarioValido;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarEstadoUsuario(Connection conexion, UsuarioDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE usuario SET usua_estado = ? WHERE usua_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(2, datos.getId(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param usuaId
     * @param contrasenia
     * @return
     *
     */
    public boolean generarContraseña(Connection conexion, String usuaId, String contrasenia) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append("UPDATE usuario SET usua_contrasenia = SHA2(?,256) WHERE  usua_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, contrasenia, ps);
            AsignaAtributoStatement.setString(2, usuaId, ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarUsuario(Connection conexion, UsuarioDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE usuario SET usua_estado = ?, tius_id = ? WHERE usua_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(2, datos.getIdTipoUsuario(), ps);
            AsignaAtributoStatement.setString(3, datos.getId(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }
}
