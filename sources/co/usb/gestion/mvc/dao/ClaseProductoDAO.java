/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.ClaseProductoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class ClaseProductoDAO {

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarClaseProducto(Connection conexion, ClaseProductoDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            System.out.println(datos.toStringJson());
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO clase_producto (clpr_descripcion, clpr_codigo, supr_id, clpr_estado) ");
            cadSQL.append(" VALUES (?, ?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(2, datos.getCodigo(), ps);
            AsignaAtributoStatement.setString(3, datos.getIdSubTipoProducto(), ps);
            AsignaAtributoStatement.setString(4, datos.getEstado(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    datos.setId(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<ClaseProductoDTO> listarClaseProducto(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList listado = null;
        ClaseProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.clpr_id, c.clpr_descripcion, c.clpr_codigo, c.supr_id, c.clpr_estado, s.supr_descripcion, t.tipr_id, t.tipr_descripcion FROM clase_producto as c ");
            cadSQL.append(" INNER JOIN subtipo_producto s ON s.supr_id = c.supr_id  ");
            cadSQL.append(" INNER JOIN tipo_producto t ON t.tipr_id = s.tipr_id ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new ClaseProductoDTO();
                datos.setId(rs.getString("c.clpr_id"));
                datos.setDescripcion(rs.getString("c.clpr_descripcion"));
                datos.setCodigo(rs.getString("c.clpr_codigo"));
                datos.setEstado(rs.getString("c.clpr_estado"));
                datos.setIdSubTipoProducto(rs.getString("c.supr_id"));
                datos.setSubtipoProducto(rs.getString("s.supr_descripcion"));
                datos.setIdTipoProducto(rs.getString("t.tipr_id"));
                datos.setTipoProducto(rs.getString("t.tipr_descripcion"));
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return listado;
    }
    
    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<ClaseProductoDTO> listarClaseProductoSub(Connection conexion,String idSubProducto ) {
        System.out.println("#######" + idSubProducto);
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList listado = null;
        ClaseProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT DISTINCT c.clpr_clase FROM clase_producto as c ");
            cadSQL.append(" INNER JOIN subtipo_producto s ON s.supr_id = c.supr_id  ");
            cadSQL.append(" INNER JOIN tipo_producto t ON t.tipr_id = s.tipr_id where s.tipr_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idSubProducto, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new ClaseProductoDTO();
                datos.setCclase(rs.getString("c.clpr_clase"));
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return listado;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarClaseProducto(Connection conexion, ClaseProductoDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE clase_producto SET clpr_descripcion = ?, clpr_codigo = ?, supr_id = ?, clpr_estado = ? WHERE clpr_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(2, datos.getCodigo(), ps);
            AsignaAtributoStatement.setString(3, datos.getIdSubTipoProducto(), ps);
            AsignaAtributoStatement.setString(4, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(5, datos.getId(), ps);
            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }
}
