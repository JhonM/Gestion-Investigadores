/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.constantes;

/**
 *
 * @author Sys. Hebert Medelo
 */
public interface Usuarios {

    // -----------------------------------------------------------
    // Constantes TIPOS DE USUARIO
    // -----------------------------------------------------------    
    public static final String ADMINISTRADOR = "1";

    // -----------------------------------------------------------
    // Constantes Estado DE USUARIO
    // -----------------------------------------------------------      
    public static final String ESTADO_ACTIVO = "1";
    public static final String ESTADO_INACTIVO = "0";

}
