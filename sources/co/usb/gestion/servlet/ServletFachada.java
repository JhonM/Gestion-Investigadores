/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.servlet;

import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.fachada.FachadaAppGestion;
import co.usb.gestion.mvc.fachada.FachadaSeguridad;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author Sys. Hebert Medelo
 */
@WebServlet(name = "ServletFachada", urlPatterns = {"/servletFachada"}, loadOnStartup = 1)
public class ServletFachada extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {

        super.init(config);

        LoggerMessage logMsg = LoggerMessage.getInstancia();

        try {

            logMsg = LoggerMessage.getInstancia();

            ServletContext contexto = this.getServletContext();

            FachadaSeguridad fachadaSeguridad = new FachadaSeguridad();
            contexto.setAttribute("fachadaSeguridad", fachadaSeguridad);

            FachadaAppGestion fachadaAppGestion = new FachadaAppGestion();
            contexto.setAttribute("fachadaAppGestion", fachadaAppGestion);

            logMsg.loggerMessageDebug("FachadaSeguridad cargada en el contexto de la aplicacion");

        } catch (Exception e) {
            logMsg.loggerMessageException(e);
        }

    }

    @Override
    public void destroy() {
    }
}
